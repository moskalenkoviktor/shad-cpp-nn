#include <cstdint>
#include <iostream>

struct S {
    bool flag;
    uint64_t x;
    uint16_t order;
    uint64_t y;
    S* child;
    S* parent;
    char data[5];
    uint64_t z;
};

int main() {
    std::cout << sizeof(S) << std::endl;
    return 0;
}