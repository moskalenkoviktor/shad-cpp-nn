add_gtest(test_sync_map sync_map_test.cpp)

target_link_libraries(test_sync_map libhazard_ptr)

add_benchmark(bench_sync_map sync_map_bench.cpp)

target_link_libraries(bench_sync_map libhazard_ptr)
