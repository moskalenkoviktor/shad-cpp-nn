#include <gtest/gtest.h>

#include <thread>
#include <atomic>

#include <timerqueue.h>

using namespace std::chrono_literals;

TEST(TimerQueue, AddGet) {
    auto now = std::chrono::system_clock::now();

    TimerQueue<int> queue;
    queue.Add(0, now + 1ms);
    queue.Add(1, now + 10ms);
    queue.Add(2, now + 5ms);

    ASSERT_EQ(0, queue.Pop());
    ASSERT_EQ(2, queue.Pop());
    ASSERT_EQ(1, queue.Pop());
}

TEST(TimerQueue, Blocking) {
    auto now = std::chrono::system_clock::now();

    TimerQueue<int> queue;
    queue.Add(0, now + 500ms);
    queue.Pop();

    ASSERT_TRUE(std::chrono::system_clock::now() >= now + 500ms);
}

TEST(TimerQueue, ManyThreads) {
    auto now = std::chrono::system_clock::now();

    TimerQueue<int> queue;

    std::atomic<bool> finished = false;
    std::thread worker([&] {
        queue.Pop();
        finished = true;
    });

    std::this_thread::sleep_for(500ms);
    EXPECT_FALSE(finished);

    queue.Add(0, now);
    worker.join();
}

TEST(TimerQueue, TimerReschedule) {
    auto now = std::chrono::system_clock::now();

    TimerQueue<int> queue;
    queue.Add(0, now + 10s);

    std::thread worker([&] { queue.Pop(); });

    std::this_thread::sleep_for(500ms);

    queue.Add(1, now);
    worker.join();

    ASSERT_TRUE(std::chrono::system_clock::now() < now + 1s);
}
